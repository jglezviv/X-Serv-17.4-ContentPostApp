#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\n')[-1]


        return (metodo,recurso,cuerpo)

    def process(self, petition):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        metodo,recurso,cuerpo = petition

        form = """
            <br/><br/>
            <form action="" method = "POST"> 
            <input type="text" name="name" value="">
            <input type="submit" value="Send">
             </form>
        """
        if metodo == "GET":
            if recurso in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body> The resource was found! You requested: " + recurso + " es : "+ self.content[recurso] \
                    + "<br>" + " Do you want to add new resources? :" + form + "</body></html>"
            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>Not Found." + "<br>" + "Do you want to add new resources? :" + form + "</body></html>"
        else:
            self.content[recurso] = cuerpo
            httpCode = "200 OK"
            htmlBody = "<html><body> added " + recurso + " with description " + self.content[recurso]+ \
                       "Do you want to add new resources? :" \
                       + "<br>" + form + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
